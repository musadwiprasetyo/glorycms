<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\TestimoniController;
use App\Http\Controllers\FAQController;
use App\Http\Controllers\LegalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/manage/login', [LoginController::class, 'index'])->name('manage.login');
Route::post('/manage/login', [LoginController::class, 'checkLogin'])->name('manage.checklogin');
Route::group(['middleware' => ['auth','acl:web']],function(){
	Route::get('/manage', [BerandaController::class, 'index'])->name('manage.beranda');
	Route::get('/manage/logout', [LoginController::class, 'logout'])->name('manage.logout');
	

    // BLOG
    Route::get('manage/blog',[BlogController::class, 'index'])->name('blog.index');
    Route::post('manage/blog/getdata', [BlogController::class, 'getData'])->name('blog.getdata');
    Route::get('manage/blog/tambah',[BlogController::class, 'tambah'])->name('blog.tambah');
    Route::get('manage/blog/detail/{id}',[BlogController::class, 'detail'])->name('blog.detail');
    Route::get('manage/blog/ubah/{id}',[BlogController::class, 'ubah'])->name('blog.ubah');
    Route::delete('manage/blog/hapus/{id?}',[BlogController::class, 'hapus'])->name('blog.hapus');
    Route::post('manage/blog/simpan',[BlogController::class, 'simpan'])->name('blog.simpan');

	// STAFF
    Route::get('manage/staff',[StaffController::class, 'index'])->name('staff.index');
    Route::post('manage/staff/getdata', [StaffController::class, 'getData'])->name('staff.getdata');
    Route::get('manage/staff/tambah',[StaffController::class, 'tambah'])->name('staff.tambah');
    Route::get('manage/staff/detail/{id}',[StaffController::class, 'detail'])->name('staff.detail');
    Route::get('manage/staff/ubah/{id}',[StaffController::class, 'ubah'])->name('staff.ubah');
    Route::delete('manage/staff/hapus/{id?}',[StaffController::class, 'hapus'])->name('staff.hapus');
    Route::post('manage/staff/simpan',[StaffController::class, 'simpan'])->name('staff.simpan');


    //SLIDER
    Route::get('manage/slider',[SliderController::class, 'index'])->name('slider.index');
    Route::post('manage/slider/getdata', [SliderController::class, 'getData'])->name('slider.getdata');
    Route::get('manage/slider/tambah',[SliderController::class, 'tambah'])->name('slider.tambah');
    Route::get('manage/slider/ubah/{id}',[SliderController::class, 'ubah'])->name('slider.ubah');
    Route::delete('manage/slider/hapus/{id?}',[SliderController::class, 'hapus'])->name('slider.hapus');
    Route::post('manage/slider/simpan',[SliderController::class, 'simpan'])->name('slider.simpan');
    Route::get('manage/slider/sorting/{id?}',[SliderController::class, 'sorting'])->name('slider.sorting');

    //TESTIMONI
    Route::get('manage/testimoni',[TestimoniController::class, 'index'])->name('testimoni.index');
    Route::post('manage/testimoni/getdata', [TestimoniController::class, 'getData'])->name('testimoni.getdata');
    Route::get('manage/testimoni/tambah',[TestimoniController::class, 'tambah'])->name('testimoni.tambah');
    Route::get('manage/testimoni/ubah/{id}',[TestimoniController::class, 'ubah'])->name('testimoni.ubah');
    Route::delete('manage/testimoni/hapus/{id?}',[TestimoniController::class, 'hapus'])->name('testimoni.hapus');
    Route::post('manage/testimoni/simpan',[TestimoniController::class, 'simpan'])->name('testimoni.simpan');
    Route::get('manage/testimoni/sorting/{id?}',[TestimoniController::class, 'sorting'])->name('testimoni.sorting');


    //FAQ
    Route::get('manage/faq',[FAQController::class, 'index'])->name('faq.index');
    Route::post('manage/faq/getdata', [FAQController::class, 'getData'])->name('faq.getdata');
    Route::get('manage/faq/tambah',[FAQController::class, 'tambah'])->name('faq.tambah');
    Route::get('manage/faq/ubah/{id}',[FAQController::class, 'ubah'])->name('faq.ubah');
    Route::delete('manage/faq/hapus/{id?}',[FAQController::class, 'hapus'])->name('faq.hapus');
    Route::post('manage/faq/simpan',[FAQController::class, 'simpan'])->name('faq.simpan');
    Route::get('manage/faq/sorting/{id?}',[FAQController::class, 'sorting'])->name('faq.sorting');


    //LEGAL
    Route::get('manage/legal',[LegalController::class, 'index'])->name('legal.index');
    Route::post('manage/legal/getdata', [LegalController::class, 'getData'])->name('legal.getdata');
    Route::get('manage/legal/tambah',[LegalController::class, 'tambah'])->name('legal.tambah');
    Route::get('manage/legal/ubah/{id}',[LegalController::class, 'ubah'])->name('legal.ubah');
    Route::post('manage/legal/simpan',[LegalController::class, 'simpan'])->name('legal.simpan');
   
    // MEMBER
    Route::get('manage/member',[MemberController::class, 'index'])->name('member.index');
    Route::post('manage/member/getdata', [MemberController::class, 'getData'])->name('member.getdata');
    Route::get('manage/member/tambah',[MemberController::class, 'tambah'])->name('member.tambah');
    Route::get('manage/member/detail/{id}',[MemberController::class, 'detail'])->name('member.detail');
    Route::get('manage/member/ubah/{id}',[MemberController::class, 'ubah'])->name('member.ubah');
    Route::delete('manage/member/hapus/{id?}',[MemberController::class, 'hapus'])->name('member.hapus');
    Route::post('manage/member/simpan',[MemberController::class, 'simpan'])->name('member.simpan');


    //ABOUT
    Route::get('manage/tentang',[AboutController::class, 'form'])->name('about.index');
    Route::post('manage/tentang/simpan',[AboutController::class, 'simpan'])->name('about.simpanumum');
    Route::post('manage/tentang/simpaninfo',[AboutController::class, 'simpanInfo'])->name('about.simpaninfo');
    Route::post('manage/tentang/simpanmedia',[AboutController::class, 'simpanMedia'])->name('about.simpanmedia');

   //PERMISSION
    Route::get('manage/permission',[PermissionController::class, 'index'])->name('permission.index');
    Route::get('manage/permission/tambah',[PermissionController::class, 'tambah'])->name('permission.tambah');
    Route::get('manage/permission/ubah/{id}',[PermissionController::class, 'ubah'])->name('permission.ubah');
    Route::post('manage/permission/simpan/{id?}',[PermissionController::class, 'simpan'])->name('permission.simpan');
    Route::get('manage/permission/sidebar',[PermissionController::class, 'sidebar'])->name('permission.sidebar');
   
    //ROLE
    Route::get('manage/role',[RoleController::class, 'index'])->name('role.index');
    Route::get('manage/role/lihat/{id}',[RoleController::class, 'lihat'])->name('role.lihat');
    Route::get('manage/role/tambah',[RoleController::class, 'form'])->name('role.tambah');
    Route::get('manage/role/ubah/{id}',[RoleController::class, 'form'])->name('role.ubah');
    Route::get('manage/role/user/{id}',[RoleController::class, 'formuser'])->name('role.user');
    Route::post('manage/role/tambah',[RoleController::class, 'save'])->name('role.tambah');
    Route::post('manage/role/ubah/{id}',[RoleController::class, 'save'])->name('role.ubah');
    Route::post('manage/role/user/{id}',[RoleController::class, 'saveuser'])->name('role.user');
    Route::post('manage/role/getdata', [RoleController::class, 'getData'])->name('role.getdata');
    Route::delete('manage/role/hapus/{id?}',[RoleController::class, 'delete'])->name('role.hapus');

    //PROFILE
    Route::get('manage/profil',[StaffController::class, 'profil'])->name('profil.index');
    Route::post('manage/profil/simpan',[StaffController::class, 'profilSimpan'])->name('profil.simpan');
    Route::get('manage/newpassword',[StaffController::class, 'profilPassword'])->name('profil.password');
    Route::post('manage/password/simpan',[StaffController::class, 'profilNewPassword'])->name('profil.simpanpassword');


});