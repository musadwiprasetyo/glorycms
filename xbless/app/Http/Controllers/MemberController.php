<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Auth;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;

class MemberController extends Controller
{
    protected $original_column = array(
        1 => "profil",
        2 => "name",
        3 => "jk",
        4 => "email",
        5 => "no_hp",
        6 => "active",
      );
      public function status()
      {
         $value = array('1'=>'Aktif' ,'0'=>'Tidak Aktif','2'=>'Blokir');
        return $value;
      }
      public function statusFilter()
      {
         $value = array('99'=>'Semua','1'=>'Aktif' ,'0'=>'Tidak Aktif','2'=>'Blokir');
        return $value;
      }
      //index :: function get list staff/user
      public function index()
      {
         $statusfilter = $this->statusFilter();
          if(session('statusfilter')==""){
            $selectedstatusfilter = "99";
          }else if(session('statusfilter')=="99"){
             $selectedstatusfilter = "99";
          }else{
            $selectedstatusfilter = session('statusfilter');
          }
          return view('backend/member/index',compact('selectedstatusfilter','statusfilter'));
      }
      // getData :: function get data staff/user
      public function getData(Request $request)
      {
          $limit = $request->length;
          $start = $request->start;
          $page  = $start +1;
          $search = $request->search['value'];

          $request->session()->put('statusfilter', $request->statusfilter);

          $query = Member::select('id','name','email','jk','profil','nohp','status','last_login_at','created_at');
          if($request->statusfilter !='99'){
              $query->where('status',$request->statusfilter);
          }

          if(array_key_exists($request->order[0]['column'], $this->original_column)){
             $query->orderByRaw($this->original_column[$request->order[0]['column']].' '.$request->order[0]['dir']);
          }
           else{
            $query->orderBy('id','DESC');
          }
           if($search) {
            $query->where(function ($query) use ($search) {
                    $query->orWhere('name','LIKE',"%{$search}%");
                    $query->orWhere('email','LIKE',"%{$search}%");
            });
          }
          $totalData = $query->get()->count();
          // Filtered
          $totalFiltered = $query->get()->count();
          // Paginate
          $query->limit($limit);
          $query->offset($start);
          $data = $query->get();
          foreach ($data as $key=> $result)
          {
            $enc_id = $this->safe_encode(Crypt::encryptString($result->id));
            $action = "";
           
            $action.="";
         
            if($request->user()->can('member.detail')){
              $action.='<a href="'.route('member.detail',$enc_id).'" class="btn btn-success btn-xs icon-btn md-btn-flat product-tooltip" title="Detail" data-original-title="Show"><i class="ion ion-md-eye"></i></a>&nbsp';
            }
            if($request->user()->can('member.ubah')){
              $action.='<a href="'.route('member.ubah',$enc_id).'" class="btn btn-warning btn-xs icon-btn md-btn-flat product-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>&nbsp;';
            }
            if($request->user()->can('member.hapus')){
              $action.='<a href="#" onclick="deleteData(this,\''.$enc_id.'\')" class="btn btn-danger btn-xs icon-btn md-btn-flat product-tooltip" title="Hapus"><i class="ion ion-md-close"></i></a>&nbsp;';
            }
            if ($result->status=='1') {
               $status = '<span class="badge badge-outline-success">Aktif</span>';
            }else if($result->status=='0'){
               $status = '<span class="badge badge-outline-danger">Tidak Aktif</span>';
            }else if($result->status=='2'){
               $status = '<span class="badge badge-outline-default">Blokir</span>';
            }

            if ($result->profil==null) {
              $profile=$this->defaultProfilePhotoUrl($result->name);
            }else{
              $profile=url($result->profil);
            }

            $result->no             = $key+$page;
            $result->photo         = '<div class="media align-items-center"><img class="ui-w-40 d-block" src="'.$profile.'" alt=""></div>';
            $result->id             = $result->id;
            $result->name           = $result->name;
            $result->email          = $result->email;
            $result->last_login_at  = $result->last_login_at;
            $result->status         = $status;
            $result->action         = $action;
          }
          if ($request->user()->can('member.index')) {
            $json_data = array(
                      "draw"            => intval($request->input('draw')),  
                      "recordsTotal"    => intval($totalData),  
                      "recordsFiltered" => intval($totalFiltered), 
                      "data"            => $data
                      );
          }else{
             $json_data = array(
                      "draw"            => intval($request->input('draw')),  
                      "recordsTotal"    => 0,  
                      "recordsFiltered" => 0, 
                      "data"            => []  
                      );
          }    
          return json_encode($json_data); 
      }
     
    
      private function cekExist($column,$var,$id)
      {
       $cek = Member::where('id','!=',$id)->where($column,'=',$var)->first();
       return (!empty($cek) ? false : true);
      }
      
      function safe_encode($string) {
	
        $data = str_replace(array('/'),array('_'),$string);
        return $data;
     }
 
	  function safe_decode($string,$mode=null) {
		
		   $data = str_replace(array('_'),array('/'),$string);
        return $data;
     }
      // tambah: Form Tambah staff
      public function tambah()
      {
        $status= $this->status();
        $selectedstatus   = '1';
        return view('backend/member/form',compact('status','selectedstatus'));
      }
      // ubah : Form ubah data
      public function ubah($enc_id)
      {
        $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
       
         
        if ($dec_id) {
          $status= $this->status();
          $member= Member::find($dec_id);
          $selectedstatus   =  $member->status;
          if ($member->profil==null) {
                  	 
          	$profile=$this->defaultProfilePhotoUrl($member->name);
          }else{
          	$profile=url($member->profil);
          }

          return view('backend/member/form',compact('status','enc_id','selectedstatus','member','profile'));
        } else {
        	return view('errors/noaccess');
        }
      }
      protected function defaultProfilePhotoUrl($name)
   	  {
        return 'https://ui-avatars.com/api/?name='.urlencode($name).'&color=ffffff&background=ed4626&rounded=true&length=3';
      }
       public function detail(Request $request,$enc_id)
      {
        $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));  
        if ($dec_id) {
          $status= $this->status();
          $member= Member::find($dec_id);
          $selectedstatus   =  $member->status;
          if ($member->profil==null) {
                     
            $profile=$this->defaultProfilePhotoUrl($member->name);
            
          }else{
            $profile=url($member->profil);
          }
          
          if ($member->status=='1') {
             $status = '<span class="badge badge-outline-success">Aktif</span>';
          }else if($member->status=='0'){
             $status = '<span class="badge badge-outline-danger">Tidak Aktif</span>';
          }else if($member->status=='2'){
             $status = '<span class="badge badge-outline-default">Blokir</span>';
          }
          $tgl_last_login = $member->last_login_at==null? '-':Carbon::parse($member->last_login_at)->format('d/m/Y H:i');
          $tgl_registrasi = $member->created_at==null? '-':Carbon::parse($member->created_at)->format('d/m/Y H:i');
         

          return view('backend/member/detail',compact('status','enc_id','selectedstatus','member','profile','tgl_last_login','tgl_registrasi','status'));
        } else {
        	return view('errors/noaccess');
        }
      }
      // simpan : Insert or Update Data
      public function simpan(Request $req)
      {     
          $enc_id     = $req->enc_id;
          $dataprofil = $req->image;
          $dir        = 'media/member/';
          if ($enc_id != null) {
            $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
          }else{
            $dec_id = null;
          }
          $cek_mail = $this->cekExist('email',$req->email,$dec_id);

         if(!$cek_mail)
          {
              $json_data = array(
                "success"         => FALSE,
                "message"         => 'Mohon maaf. Email yang Anda masukan sudah terdaftar pada sistem.'
              );
          }
          else {
          if($enc_id){
             $member = Member::find($dec_id);
             
             if ($dataprofil != null) {
              list($type, $dataprofil) = explode(';', $dataprofil);
              list(, $dataprofil)      = explode(',', $dataprofil);
              if(!file_exists($dir)){
                  mkdir($dir, 0777, true);
                   chmod($dir, 0777);
              }
              $dataprofil = base64_decode($dataprofil);
              $image_name= 'profile_'.time().'.png';
              $path = $dir. $image_name;
              $paths  = url($dir . $image_name);
              file_put_contents($path, $dataprofil);
              chmod($path, 0664);
              if ($member->profil != null) {
                $db_path = $member->profil;
                if (file_exists($db_path)) {
                  unlink($db_path);
                }
              }
              $member->profil      = $path;
            }      
            $member->name       = $req->name;
            $member->email      = $req->email;
            if ($req->password !='') {
                    $member->password   = bcrypt($req->password);
            }
            $member->nohp        = $req->nohp;
            $member->jk          = $req->jk;
            $member->status      = $req->status;
            $member->save();
            if ($member) {
              $json_data = array(
                    "success"         => TRUE,
                    "message"         => 'Data berhasil diperbarui.'
                 );
            }else{
               $json_data = array(
                    "success"         => FALSE,
                    "message"         => 'Data gagal diperbarui.'
                 );
            }
            
          }else{

            if ($dataprofil != null) {
               
                list($type, $dataprofil) = explode(';', $dataprofil);
                list(, $dataprofil)      = explode(',', $dataprofil);
              

                if(!file_exists($dir)){
                    mkdir($dir, 0777, true);
                     chmod($dir, 0777);
                }
                 
                $dataprofil = base64_decode($dataprofil);
                $image_name= 'profile_'.time().'.png';
                $path = $dir. $image_name;
                $paths  = url($dir . $image_name);
                file_put_contents($path, $dataprofil);
                chmod($path, 0664);
          }else{
            $path = null;
          }

            $member = new Member;
            $member->name        = $req->name;
            $member->email       = $req->email;
            $member->password    = bcrypt($req->password);
            $member->nohp        = $req->nohp;
            $member->jk          = $req->jk;
            $member->profil      = $path;
            $member->active      = $req->status;
            $member->save();
            if($member) {
              $json_data = array(
                    "success"         => TRUE,
                    "message"         => 'Data berhasil ditambahkan.'
              );
            }else{
              $json_data = array(
                    "success"         => FALSE,
                    "message"         => 'Data gagal ditambahkan.'
              );
            }
            
          }
        }
        return json_encode($json_data); 
      }
      public function hapus(Request $req,$enc_id)
      {
        $dec_id   = $this->safe_decode(Crypt::decryptString($enc_id));
        $member    = Member::find($dec_id);
        $cekexist = RoleUser::where('user_id',$dec_id)->first();
        if($member) {
            
            	if ($member->profil != null) {
	                $db_path = $member->profil;
	                if (file_exists($db_path)) {
	                  unlink($db_path);
	                }
	             }
                $member->delete();
                return response()->json(['status'=>"success",'message'=>'Data Berhasil dihapus.']);
            
        }else {
            return response()->json(['status'=>"failed",'message'=>'Gagal menghapus data. Silahkan ulangi kembali.']);
        }
      }

}
