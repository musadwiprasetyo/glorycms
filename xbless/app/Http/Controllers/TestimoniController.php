<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Testimoni;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use DB;
class TestimoniController extends Controller
{
   protected $original_column = array(
    1 => "name",
    2 => "job",
    3 => "sort",
    4 => "status",
  );
  public function status()
  {

    $value = array('1'=>'Aktif' ,'0'=>'Tidak Aktif' );
    return $value;
  }
     
  public function index()
  {
    return view('backend/testimoni/index');
  }
  
  public function getData(Request $request)
  {
      $limit = $request->length;
      $start = $request->start;
      $page  = $start +1;
      $search = $request->search['value'];

      $records = Testimoni::select('*');

      if(array_key_exists($request->order[0]['column'], $this->original_column)){
         $records->orderByRaw($this->original_column[$request->order[0]['column']].' '.$request->order[0]['dir']);
      }
      else{
        $records->orderBy('sort','ASC');
      }
       if($search) {
        $records->where(function ($query) use ($search) {
                $query->orWhere('name','LIKE',"%{$search}%");
                $query->orWhere('job','LIKE',"%{$search}%");
        });
      }
      $totalData = $records->get()->count();
     
      $totalFiltered = $records->get()->count();
    
      $records->limit($limit);
      $records->offset($start);
      $data = $records->get();
      foreach ($data as $key=> $record)
      {
        $enc_id = $this->safe_encode(Crypt::encryptString($record->id));
        $action = "";
       	if($request->user()->can('testimoni.detail')){
           $action.='<a href="#" onclick="detailData(this,'.$key.')" class="btn btn-success btn-xs icon-btn md-btn-flat product-tooltip" title="Detail"><i class="ion ion-md-eye"></i></a>&nbsp;';
        }
        if($request->user()->can('testimoni.ubah')){
          $action.='<a href="'.route('testimoni.ubah',$enc_id).'" class="btn btn-warning btn-xs icon-btn md-btn-flat product-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>&nbsp;';
        }
        if($request->user()->can('testimoni.hapus')){
          $action.='<a href="#" onclick="deleteData(this,\''.$enc_id.'\')" class="btn btn-danger btn-xs icon-btn md-btn-flat product-tooltip" title="Hapus"><i class="ion ion-md-close"></i></a>&nbsp;';
        }

        

             
        $record->no             = $key+$page;
        $record->DT_RowId       = $record->id;
        $record->id             = $record->id;
        $record->name           = $record->name;
        $record->tgl            = date('d-m-Y',strtotime($record->created_at));
       	$record->status         = $record->status=='1' ? '<span class="badge badge-outline-success">Aktif</span>' : '<span class="badge badge-outline-danger">Tidak Aktif</span>' ;
        $record->action         = $action;
      }
      if ($request->user()->can('testimoni.index')) {
        $json_data = array(
                  "draw"            => intval($request->input('draw')),  
                  "recordsTotal"    => intval($totalData),  
                  "recordsFiltered" => intval($totalFiltered), 
                  "data"            => $data   
                  );
      }else{
         $json_data = array(
                  "draw"            => intval($request->input('draw')),  
                  "recordsTotal"    => 0,  
                  "recordsFiltered" => 0, 
                  "data"            => []  
                  );
      }    
      return json_encode($json_data); 
  }
  function safe_encode($string) {
    $data = str_replace(array('/'),array('_'),$string);
    return $data;
  }
  function safe_decode($string,$mode=null) {
    $data = str_replace(array('_'),array('/'),$string);
    return $data;
  }
  public function tambah()
  {
      $status = $this->status();
      $selectedstatus = "1";
      return view('backend/testimoni/form',compact('status','selectedstatus'));
  }
  // ubah : Form ubah data
  public function ubah($enc_id)
  {
    $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));   
      if ($dec_id) {
        $testimoni= Testimoni::find($dec_id);
        $status = $this->status();
        $selectedstatus = $testimoni->status;
        
        return view('backend/testimoni/form',compact('enc_id','testimoni','status','selectedstatus'));
      } else {
        return view('errors/noaccess');
      }
    }
    public function simpan(Request $req)
    {     
      $enc_id     = $req->enc_id;

    

      if ($enc_id != null) {
        $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
      }else{
        $dec_id = null;
      }

      if($enc_id){
        
        $testimoni = Testimoni::find($dec_id);
        
        $testimoni->name      = $req->name;
        $testimoni->job       = $req->job;
        $testimoni->content   = $req->content;
        $testimoni->status    = $req->status;
        $testimoni->updated_by= Auth()->user()->name;
        $testimoni->save();
        if ($testimoni) {
          $json_data = array(
                "success"         => TRUE,
                "message"         => 'Data berhasil diperbarui.'
             );
        }else{
           $json_data = array(
                "success"         => FALSE,
                "message"         => 'Data gagal diperbarui.'
             );
        }
      }else{

        $testimoni = new Testimoni;
      
        $testimoni->name      = $req->name;
        $testimoni->job       = $req->job;
        $testimoni->content   = $req->content;
        $testimoni->status    = $req->status;
        $newsort              = DB::table('testimonial')->max('sort');
        $testimoni->sort      = $newsort+1;
        $testimoni->created_by= Auth()->user()->name;
        $testimoni->save();
        
        if($testimoni) {
          $json_data = array(
                "success"         => TRUE,
                "message"         => 'Data berhasil ditambahkan.'
          );
        }else{
          $json_data = array(
                "success"         => FALSE,
                "message"         => 'Data gagal ditambahkan.'
          );
        }

      }
      return json_encode($json_data);
    }
    public function hapus(Request $req,$enc_id)
    {
      $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
      $testimoni = Testimoni::find($dec_id);
    
      if ($testimoni) {
          $testimoni->delete();
          return response()->json(['status'=>"success",'message'=>'Testimoni berhasil dihapus.']);
      }else{
           return response()->json(['status'=>"failed",'message'=>'Gagal menghapus data']);
      }
    }
    public function sorting(Request $request,$id){
     $this->doSorting($request->input('sorting'));
     return response()->json(['success'=>true]);
    }
    public function doSorting($data){
       for ($i=0; $i < sizeof($data); $i++) { 
        $testimoni    = Testimoni::find($data[$i]['id']);
        $testimoni->sort  = $data[$i]['sort'];
        $testimoni->save();
       }
    }
}