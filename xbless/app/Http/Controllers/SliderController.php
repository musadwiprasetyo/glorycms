<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use DB;
class SliderController extends Controller
{
    protected $original_column = array(
    1 => "title",
    2 => "sort",
    3 => "link",
    4 => "status",
  );
  public function status()
  {

    $value = array('1'=>'Aktif' ,'0'=>'Tidak Aktif' );
    return $value;
  }
     
  public function index()
  {
    return view('backend/slider/index');
  }
  
  public function getData(Request $request)
  {
      $limit = $request->length;
      $start = $request->start;
      $page  = $start +1;
      $search = $request->search['value'];

      $records = Slider::select('*');

      if(array_key_exists($request->order[0]['column'], $this->original_column)){
         $records->orderByRaw($this->original_column[$request->order[0]['column']].' '.$request->order[0]['dir']);
      }
      else{
        $records->orderBy('sort','ASC');
      }
       if($search) {
        $records->where(function ($query) use ($search) {
                $query->orWhere('title','LIKE',"%{$search}%");
        });
      }
      $totalData = $records->get()->count();
     
      $totalFiltered = $records->get()->count();
    
      $records->limit($limit);
      $records->offset($start);
      $data = $records->get();
      foreach ($data as $key=> $record)
      {
        $enc_id = $this->safe_encode(Crypt::encryptString($record->id));
        $action = "";
       	if($request->user()->can('slider.detail')){
           $action.='<a href="#" onclick="detailData(this,'.$key.')" class="btn btn-success btn-xs icon-btn md-btn-flat product-tooltip" title="Detail"><i class="ion ion-md-eye"></i></a>&nbsp;';
        }
        if($request->user()->can('slider.ubah')){
          $action.='<a href="'.route('slider.ubah',$enc_id).'" class="btn btn-warning btn-xs icon-btn md-btn-flat product-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>&nbsp;';
        }
        if($request->user()->can('member.hapus')){
          $action.='<a href="#" onclick="deleteData(this,\''.$enc_id.'\')" class="btn btn-danger btn-xs icon-btn md-btn-flat product-tooltip" title="Hapus"><i class="ion ion-md-close"></i></a>&nbsp;';
        }

        

             
        $record->no             = $key+$page;
        $record->id             = $record->id;
        $record->title          = $record->title;
        $record->DT_RowId       = $record->id;
 
        if ($record->banner==null) {
          $record->banner        =url('media/slider/no_img.png');
        } else {
          $record->banner        = '<img src="'.url($record->banner).'" width="300px"/>';
        }
        $record->link           = $record->link;
        $record->tgl            = date('d-m-Y',strtotime($record->created_at));
       	$record->status         = $record->status=='1' ? '<span class="badge badge-outline-success">Aktif</span>' : '<span class="badge badge-outline-danger">Tidak Aktif</span>' ;
        $record->action         = $action;
      }
      if ($request->user()->can('slider.index')) {
        $json_data = array(
                  "draw"            => intval($request->input('draw')),  
                  "recordsTotal"    => intval($totalData),  
                  "recordsFiltered" => intval($totalFiltered), 
                  "data"            => $data   
                  );
      }else{
         $json_data = array(
                  "draw"            => intval($request->input('draw')),  
                  "recordsTotal"    => 0,  
                  "recordsFiltered" => 0, 
                  "data"            => []  
                  );
      }    
      return json_encode($json_data); 
  }
  function safe_encode($string) {
    $data = str_replace(array('/'),array('_'),$string);
    return $data;
  }
  function safe_decode($string,$mode=null) {
    $data = str_replace(array('_'),array('/'),$string);
    return $data;
  }
  public function tambah()
  {
      $status = $this->status();
      $selectedstatus = "1";
      return view('backend/slider/form',compact('status','selectedstatus'));
  }
  // ubah : Form ubah data
  public function ubah($enc_id)
  {
    $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));   
      if ($dec_id) {
        $slider= Slider::find($dec_id);
        $status = $this->status();
        $selectedstatus = $slider->status;
        if ($slider->banner==null) {
          $gambar=url('media/slider/no_img.png');
        } else {
          $gambar=url($slider->banner);
        }
        
        return view('backend/slider/form',compact('enc_id','slider','gambar','status','selectedstatus'));
      } else {
        return view('errors/noaccess');
      }
    }
    public function simpan(Request $req)
    {     
      $enc_id     = $req->enc_id;

      $datagambar = $req->banner;

      $dir        = 'media/slider/';

      if ($enc_id != null) {
        $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
      }else{
        $dec_id = null;
      }

      if($enc_id){
        
        $slider = Slider::find($dec_id);
        if ($datagambar != null) {
           
            list($type, $datagambar) = explode(';', $datagambar);
            list(, $datagambar)      = explode(',', $datagambar);
            if(!file_exists($dir)){
                mkdir($dir, 0777, true);
                 chmod($dir, 0777);
            }
               
            $datagambar = base64_decode($datagambar);
            $image_name= 'slider_'.time().'.png';
            $path = $dir. $image_name;
            
            file_put_contents($path, $datagambar);
            chmod($path, 0664);
            if ($slider->banner != 'media/slider/no_img.png') {
              $db_path = $slider->banner;
              if (file_exists($db_path)) {
                unlink($db_path);
              }
            }

            $slider->banner      = $path;
        }      

        $slider->title      = $req->title;
        $slider->link       = $req->link;
        $slider->status     = $req->status;
        $slider->updated_by = Auth()->user()->name;
        $slider->save();
        if ($slider) {
          $json_data = array(
                "success"         => TRUE,
                "message"         => 'Data berhasil diperbarui.'
             );
        }else{
           $json_data = array(
                "success"         => FALSE,
                "message"         => 'Data gagal diperbarui.'
             );
        }
      }else{

        $slider = new Slider;
        if ($datagambar != null) {
          
            list($type, $datagambar) = explode(';', $datagambar);
            list(, $datagambar)      = explode(',', $datagambar);
            if(!file_exists($dir)){
                mkdir($dir, 0777, true);
                 chmod($dir, 0777);
            }
               
            $datagambar = base64_decode($datagambar);
            $image_name= 'slider_'.time().'.png';
            $path = $dir. $image_name;
            
            file_put_contents($path, $datagambar);
            chmod($path, 0664);
        }else{
           $path="media/slider/no_img.png";
        }

        $slider->title      = $req->title;
        $slider->banner     = $path;
        $slider->link       = $req->link;
        $slider->status     = $req->status;
        $newsort  = DB::table('slider')->max('sort');
        $slider->sort=$newsort+1;
        $slider->created_by = Auth()->user()->name;
        $slider->save();
        
        if($slider) {
          $json_data = array(
                "success"         => TRUE,
                "message"         => 'Data berhasil ditambahkan.'
          );
        }else{
          $json_data = array(
                "success"         => FALSE,
                "message"         => 'Data gagal ditambahkan.'
          );
        }

      }
      return json_encode($json_data);
    }
    public function hapus(Request $req,$enc_id)
    {
      $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
      $slider = Slider::find($dec_id);
     
      if ($slider) {
          if ($slider->banner != 'media/slider/no_img.png') {
                $db_path = $slider->banner;
                if (file_exists($db_path)) {
                  unlink($db_path);
                }
           }
           $slider->delete();
          return response()->json(['status'=>"success",'message'=>'Slider berhasil dihapus.']);
      }else{
           return response()->json(['status'=>"failed",'message'=>'Gagal menghapus data']);
      }
    }
    public function sorting(Request $request,$id){
     $this->doSorting($request->input('sorting'));
     return response()->json(['success'=>true]);
    }

    public function doSorting($data){
       for ($i=0; $i < sizeof($data); $i++) { 
        $slide    = Slider::find($data[$i]['id']);
        $slide->sort  = $data[$i]['sort'];
        $slide->save();
       }
    }
}