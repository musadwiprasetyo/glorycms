<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;


use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use DB;
class BlogController extends Controller
{
	  public function url_blog()
  	  {
       $x = 'https://example.com/berita';
       return $x;
      }
      protected $original_column = array(
       1 => "title",
       2 => "status",
      );
	  public function status()
	  {
	    $value = array('1'=>'Aktif' ,'0'=>'Tidak Aktif' );
	    return $value;
	  }


	  public function index()
	  {
	    return view('backend/blog/index');
	  }

	  public function getData(Request $request)
	  {
	      $limit = $request->length;
	      $start = $request->start;
	      $page  = $start +1;
	      $search = $request->search['value'];

	      $records = Blog::select('*');

	      if(array_key_exists($request->order[0]['column'], $this->original_column)){
	         $records->orderByRaw($this->original_column[$request->order[0]['column']].' '.$request->order[0]['dir']);
	      }
	      else{
	        $records->orderBy('created_at','DESC');
	      }
	       if($search) {
	        $records->where(function ($query) use ($search) {
	                $query->orWhere('title','LIKE',"%{$search}%");
	        });
	      }
	      $totalData = $records->get()->count();
	     
	      $totalFiltered = $records->get()->count();
	    
	      $records->limit($limit);
	      $records->offset($start);
	      $data = $records->get();
	      foreach ($data as $key=> $record)
	      {
	        $enc_id = $this->safe_encode(Crypt::encryptString($record->id));
	        $action = "";
	       
	        $action.="";
	       
	        if($request->user()->can('blog.detail')){
              $action.='<a href="'.route('blog.detail',$enc_id).'" class="btn btn-success btn-xs icon-btn md-btn-flat product-tooltip" title="Detail" data-original-title="Show"><i class="ion ion-md-eye"></i></a>&nbsp';
            }
            if($request->user()->can('blog.ubah')){
              $action.='<a href="'.route('blog.ubah',$enc_id).'" class="btn btn-warning btn-xs icon-btn md-btn-flat product-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>&nbsp;';
            }
            if($request->user()->can('blog.hapus')){
              $action.='<a href="#" onclick="deleteData(this,\''.$enc_id.'\')" class="btn btn-danger btn-xs icon-btn md-btn-flat product-tooltip" title="Hapus"><i class="ion ion-md-close"></i></a>&nbsp;';
            }

	        $record->no             = $key+$page;
	        $record->id             = $record->id;
	        $record->title          = $record->title;
	        
	        if ($record->image==null) {
	        	$record->image        ='';
	        } else {
	        	$record->image        = '<img src="'.url($record->image).'" width="300px"/>';
	        }
	        
	        $record->metadata       = $record->metadata;
	        $record->url            = '<a href="'.$this->url_blog().'/'.$record->slug_url.'" target="_blank">'.$this->url_blog().'/'.$record->slug_url.'</a>';
	        $record->tgl            = date('d-m-Y H:i',strtotime($record->created_at));
	       	$record->status         = $record->status=='1' ? '<span class="badge badge-outline-success">Aktif</span>' : '<span class="badge badge-outline-danger">Tidak Aktif</span>' ;
	        $record->action         = $action;

	      }
	      if ($request->user()->can('blog.index')) {
	        $json_data = array(
	                  "draw"            => intval($request->input('draw')),  
	                  "recordsTotal"    => intval($totalData),  
	                  "recordsFiltered" => intval($totalFiltered), 
	                  "data"            => $data   
	                  );
	      }else{
	         $json_data = array(
	                  "draw"            => intval($request->input('draw')),  
	                  "recordsTotal"    => 0,  
	                  "recordsFiltered" => 0, 
	                  "data"            => []  
	                  );
	      }    
	      return json_encode($json_data); 
	  }
	  function safe_encode($string) {
	    $data = str_replace(array('/'),array('_'),$string);
	    return $data;
	  }
		function safe_decode($string,$mode=null) {
	    $data = str_replace(array('_'),array('/'),$string);
	    return $data;
	  }
	  public function tambah()
	  {
	      
	      $status = $this->status();
	  	  $selectedstatus = "1";
	      return view('backend/blog/form',compact('status','selectedstatus'));
	  }
	 
	    
	  public function ubah($enc_id)
	  {
	      $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
	      if ($dec_id) {
	          $blog= Blog::find($dec_id);
	    	  $status = $this->status();
	  	      $selectedstatus = $blog->status;

	          if ($blog->image==null) {
	            $gambar=url('media/blog/no_img.png');
	          } else {
	            $gambar=url($blog->image);
	          }

	          return view('backend/blog/form',compact('enc_id','blog','gambar','status','selectedstatus'));
	      } else {
	      	return view('errors/noaccess');
	      }
	   }

	   public function detail($enc_id)
	   {
	      $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
	      if ($dec_id) {
	          $blog= Blog::find($dec_id);
	    	  $status = $blog->status=='1' ? '<span class="badge badge-outline-success">Aktif</span>' : '<span class="badge badge-outline-danger">Tidak Aktif</span>' ;
	  	     
	          if ($blog->image==null) {
	            $gambar=url('media/blog/no_img.png');
	          } else {
	            $gambar=url($blog->image);
	          }

	          return view('backend/blog/detail',compact('enc_id','blog','gambar','status'));
	      } else {
	      	return view('errors/noaccess');
	      }
	   }


	   private function cekSlug($column,$var,$id) 
	   {
	      $cek = Blog::where('id','!=',$id)->where($column,'=',$var)->first();
	      return (!empty($cek) ? false : true);
	   }

	   public function simpan(Request $req)
	   {     
	      $enc_id     = $req->enc_id;
          $datagambar = $req->image;
          $dir        = 'media/member/';
	
	      if ($enc_id != null) {
	        $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
	      }else{
	        $dec_id = null;
	      }

	      if($enc_id){
	        $data = Blog::find($dec_id);

	        if ($datagambar != null) {
	              
	              
	              list($type, $datagambar) = explode(';', $datagambar);
	              list(, $datagambar)      = explode(',', $datagambar);
	           
	              if(!file_exists($dir)){
	                  mkdir($dir, 0777, true);
	                   chmod($dir, 0777);
	              }
	               
	              $datagambar = base64_decode($datagambar);
	              $image_name= 'blog_'.time().'.png';
	              $path = $dir. $image_name;
	              $paths  = url($dir . $image_name);
	              file_put_contents($path, $datagambar);
	              chmod($path, 0664);

	              if ($data->image != null) {
	                $db_path = $data->image;
	                
	                if (file_exists($db_path)) {
	                  unlink($db_path);
	                }
	              }
	              $data->image      = $path;
	        }

	        $data->title       = $req->title;
	        $data->description = $req->description;
	        $data->meta_title  = $req->meta_title;
	        $data->meta_description = $req->meta_description;
	        $data->keyword = $req->keywords;
	        $slug = Str::slug($req->title, '-');

	        $cek_slug = $this->cekSlug('slug_url',$slug,$dec_id);
	        if(!$cek_slug)
	        {
	          for($i=1;$i<=100;$i++) {
	            $new_slug = $slug.'-'.$i;
	            $existslug2 =Blog::where('slug_url',$new_slug)->first();
	            if(!$existslug2) {
	              break;
	            }
	          }
	        }
	        else {
	           $new_slug = $slug;
	        }

	        $data->slug_url    = $new_slug;
	        $data->status      = $req->status;
	        $data->updated_by  = Auth()->user()->name;
	        $data->save();

	      
	        if ($data) {
              $json_data = array(
                    "success"         => TRUE,
                    "message"         => 'Data berhasil diperbarui.'
                 );
            }else{
               $json_data = array(
                    "success"         => FALSE,
                    "message"         => 'Data gagal diperbarui.'
                 );
            }
	      }else{
	        if ($datagambar != null) {
	         
	              list($type, $datagambar) = explode(';', $datagambar);
	              list(, $datagambar)      = explode(',', $datagambar);
	            
	              if(!file_exists($dir)){
	                  mkdir($dir, 0777, true);
	                   chmod($dir, 0777);
	              }
	               
	              
	              $datagambar = base64_decode($datagambar);
	              $image_name= 'blog_'.time().'.png';
	              $path = $dir. $image_name;
	              $paths  = url($dir . $image_name);
	              file_put_contents($path, $datagambar);
	              chmod($path, 0664);
	        }else{
	          $path = 'media/blog/no_img.png';
	        }
	        $data = new Blog;
	        
	        $data->title            = $req->title;
	        $data->image            = $path;
	        $data->description      = $req->description;
	        $data->meta_title       = $req->meta_title;
	        $data->meta_description = $req->meta_description;
	        $data->keyword          = $req->keywords;

	        $slug = Str::slug($req->title, '-');
	        $cek_slug = $this->cekSlug('slug_url',$slug,$dec_id);
	        if(!$cek_slug)
	        {
	          for($i=1;$i<=100;$i++) {
	            $new_slug = $slug.'-'.$i;
	            $existslug2 =Blog::where('slug_url',$new_slug)->first();
	            if(!$existslug2) {
	              break;
	            }
	          }
	        }
	        else {
	           $new_slug = $slug;
	        }
	        $data->slug_url     = $new_slug;
	        $data->status       = $req->status;
	        $data->created_by   = Auth()->user()->name;
	        $data->save();
	        if($data) {
              $json_data = array(
                    "success"         => TRUE,
                    "message"         => 'Data berhasil ditambahkan.'
              );
            }else{
              $json_data = array(
                    "success"         => FALSE,
                    "message"         => 'Data gagal ditambahkan.'
              );
            }
	       
	      }
	      return json_encode($json_data);
	    }

	    public function hapus(Request $req,$enc_id)
	    {
	      $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
	      $blog = Blog::find($dec_id);
	     
	      if ($blog) {
	          if ($blog->image != 'media/blog/no_img.png') {
	            $db_path = $blog->image;
	            
	            if (file_exists($db_path)) {
	              unlink($db_path);
	            }
	          }
	          $blog->delete();
	          return response()->json(['status'=>"success",'message'=>'Data berhasil dihapus.']);
	      }else{
	          return response()->json(['status'=>"failed",'message'=>'Gagal menghapus data']);
	          
	      }
	    }
}

