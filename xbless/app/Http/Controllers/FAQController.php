<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FAQ;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use DB;
class FAQController extends Controller
{
   protected $original_column = array(
    1 => "question",
    2 => "sort",
  );
  public function status()
  {

    $value = array('1'=>'Aktif' ,'0'=>'Tidak Aktif' );
    return $value;
  }
     
  public function index()
  {
    return view('backend/faq/index');
  }
  
  public function getData(Request $request)
  {
      $limit = $request->length;
      $start = $request->start;
      $page  = $start +1;
      $search = $request->search['value'];

      $records = FAQ::select('*');

      if(array_key_exists($request->order[0]['column'], $this->original_column)){
         $records->orderByRaw($this->original_column[$request->order[0]['column']].' '.$request->order[0]['dir']);
      }
      else{
        $records->orderBy('sort','ASC');
      }
       if($search) {
        $records->where(function ($query) use ($search) {
                $query->orWhere('question','LIKE',"%{$search}%");
        });
      }
      $totalData = $records->get()->count();
     
      $totalFiltered = $records->get()->count();
    
      $records->limit($limit);
      $records->offset($start);
      $data = $records->get();
      foreach ($data as $key=> $record)
      {
        $enc_id = $this->safe_encode(Crypt::encryptString($record->id));
        $action = "";
       	if($request->user()->can('faq.detail')){
           $action.='<a href="#" onclick="detailData(this,'.$key.')" class="btn btn-success btn-xs icon-btn md-btn-flat product-tooltip" title="Detail"><i class="ion ion-md-eye"></i></a>&nbsp;';
        }
        if($request->user()->can('faq.ubah')){
          $action.='<a href="'.route('faq.ubah',$enc_id).'" class="btn btn-warning btn-xs icon-btn md-btn-flat product-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>&nbsp;';
        }
        if($request->user()->can('faq.hapus')){
          $action.='<a href="#" onclick="deleteData(this,\''.$enc_id.'\')" class="btn btn-danger btn-xs icon-btn md-btn-flat product-tooltip" title="Hapus"><i class="ion ion-md-close"></i></a>&nbsp;';
        }

        

             
        $record->no             = $key+$page;
        $record->DT_RowId       = $record->id;
        $record->id             = $record->id;
        $record->question       = $record->question;
        $record->answer         = $record->answer;
        $record->tgl            = date('d-m-Y',strtotime($record->created_at));
       	$record->status         = $record->status=='1' ? '<span class="badge badge-outline-success">Aktif</span>' : '<span class="badge badge-outline-danger">Tidak Aktif</span>' ;
        $record->action         = $action;
      }
      if ($request->user()->can('faq.index')) {
        $json_data = array(
                  "draw"            => intval($request->input('draw')),  
                  "recordsTotal"    => intval($totalData),  
                  "recordsFiltered" => intval($totalFiltered), 
                  "data"            => $data   
                  );
      }else{
         $json_data = array(
                  "draw"            => intval($request->input('draw')),  
                  "recordsTotal"    => 0,  
                  "recordsFiltered" => 0, 
                  "data"            => []  
                  );
      }    
      return json_encode($json_data); 
  }
  function safe_encode($string) {
    $data = str_replace(array('/'),array('_'),$string);
    return $data;
  }
  function safe_decode($string,$mode=null) {
    $data = str_replace(array('_'),array('/'),$string);
    return $data;
  }
  public function tambah()
  {
      $status = $this->status();
      $selectedstatus = "1";
      
      return view('backend/faq/form',compact('status','selectedstatus'));
  }
  // ubah : Form ubah data
  public function ubah($enc_id)
  {
    $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));   
      if ($dec_id) {
        $faq= FAQ::find($dec_id);
        $status = $this->status();
        $selectedstatus = $faq->status;
        
        return view('backend/faq/form',compact('enc_id','faq','status','selectedstatus'));
      } else {
        return view('errors/noaccess');
      }
    }
    public function simpan(Request $req)
    {     
      $enc_id     = $req->enc_id;

      if ($enc_id != null) {
        $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
      }else{
        $dec_id = null;
      }

      if($enc_id){
        
        $faq = FAQ::find($dec_id);
        
        $faq->question  = $req->question;
        $faq->answer    = $req->answer;
        $faq->status    = $req->status;
        $faq->updated_by= Auth()->user()->name;
        $faq->save();
        if ($faq) {
          $json_data = array(
                "success"         => TRUE,
                "message"         => 'Data berhasil diperbarui.'
             );
        }else{
           $json_data = array(
                "success"         => FALSE,
                "message"         => 'Data gagal diperbarui.'
             );
        }
      }else{

        $faq = new FAQ;
      
        $faq->question  = $req->question;
        $faq->answer    = $req->answer;
        $faq->status    = $req->status;
        $newsort        = DB::table('faq')->max('sort');
        $faq->sort      = $newsort+1;
        $faq->created_by= Auth()->user()->name;
        $faq->save();
        
        if($faq) {
          $json_data = array(
                "success"         => TRUE,
                "message"         => 'Data berhasil ditambahkan.'
          );
        }else{
          $json_data = array(
                "success"         => FALSE,
                "message"         => 'Data gagal ditambahkan.'
          );
        }

      }
      return json_encode($json_data);
    }
    public function hapus(Request $req,$enc_id)
    {
      $dec_id = $this->safe_decode(Crypt::decryptString($enc_id));
      $faq = FAQ::find($dec_id);
    
      if ($faq) {
          $faq->delete();
          return response()->json(['status'=>"success",'message'=>'FAQ berhasil dihapus.']);
      }else{
           return response()->json(['status'=>"failed",'message'=>'Gagal menghapus data']);
      }
    }
    public function sorting(Request $request,$id){
     $this->doSorting($request->input('sorting'));
     return response()->json(['success'=>true]);
    }
    public function doSorting($data){
       for ($i=0; $i < sizeof($data); $i++) { 
        $faq    = FAQ::find($data[$i]['id']);
        $faq->sort  = $data[$i]['sort'];
        $faq->save();
       }
    }
}