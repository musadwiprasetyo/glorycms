<div class="sidenav-divider mt-0"></div>
<ul class="sidenav-inner py-1" id="sidebar-menu">
  <li class="sidenav-item">
    <a href="{{route('manage.beranda')}}" class="sidenav-link"><i class="sidenav-icon ion ion-md-home"></i>
      <div>Beranda</div>
    </a>
  </li>
  @can('konten.index')
  <li class="sidenav-item">
    <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-md-albums"></i>
      <div>Konten</div>
    </a>
    <ul class="sidenav-menu">
      @can('blog.index')
      <li class="sidenav-item">
        <a href="{{route('blog.index')}}"  class="sidenav-link">
          <div>Blog</div>
        </a>
      </li>
      @endcan
      @can('slider.index')
      <li class="sidenav-item">
        <a href="{{route('slider.index')}}" class="sidenav-link">
          <div>Slider</div>
        </a>
      </li>
      @endcan
      @can('testimoni.index')
      <li class="sidenav-item">
        <a href="{{route('testimoni.index')}}" class="sidenav-link">
          <div>Testimoni</div>
        </a>
      </li>
      @endcan
    </ul>
  </li>
  @endcan
  @can('pengguna.index')
  <li class="sidenav-item">
    <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-md-contacts"></i>
      <div>Pengguna</div>
    </a>
    <ul class="sidenav-menu">
      @can('staff.index')
      <li class="sidenav-item">
        <a href="{{route('staff.index')}}" class="sidenav-link">
          <div>Staff</div>
        </a>
      </li>
      @endcan
      @can('member.index')
      <li class="sidenav-item">
        <a href="{{route('member.index')}}" class="sidenav-link">
          <div>Member</div>
        </a>
      </li>
      @endcan
    </ul>
  </li>
  @endcan
  @can('company.index')
  <li class="sidenav-item">
    <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-md-medical"></i>
      <div>Pengaturan</div>
    </a>
    <ul class="sidenav-menu">
      @can('about.index')
      <li class="sidenav-item">
        <a href="{{route('about.index')}}" class="sidenav-link">
          <div>Tentang</div>
        </a>
      </li>
      @endcan
      @can('faq.index')
      <li class="sidenav-item">
        <a href="{{route('faq.index')}}" class="sidenav-link">
          <div>FAQ</div>
        </a>
      </li>
      @endcan
      @can('legal.index')
      <li class="sidenav-item">
        <a href="{{route('legal.index')}}" class="sidenav-link">
          <div>Legalitas</div>
        </a>
      </li>
      @endcan
    </ul>
  </li>
  @endcan
  @can('security.index')
  <li class="sidenav-item">
    <a href="javascript:void(0)" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-md-key"></i>
      <div>Keamanan</div>
    </a>
    <ul class="sidenav-menu">
      @can('permission.index')
      <li class="sidenav-item">
        <a href="{{route('permission.index')}}" class="sidenav-link">
          <div>Modul</div>
        </a>
      </li>
      @endcan
      @can('role.index')
      <li class="sidenav-item">
        <a href="{{route('role.index')}}" class="sidenav-link">
          <div>Akses</div>
        </a>
      </li>
      @endcan
    </ul>
  </li>
  @endcan
  @can('log.index')
  <li class="sidenav-item">
    <a href="#" class="sidenav-link"><i class="sidenav-icon ion ion-md-stopwatch"></i>
      <div>Log Aktivitas</div>
    </a>
  </li>
  @endcan
</ul>