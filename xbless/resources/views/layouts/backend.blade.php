<!DOCTYPE html>
<html lang="en" class="light-style">
  <head>
    <title>@yield('pageTitle')</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/ionicons.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/linearicons.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/open-iconic.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/pe-icon-7-stroke.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/colors.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/ui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap-material-datetimepicker.css')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{isset($perusahaan)? url($perusahaan->fav) : 'https://ui-avatars.com/api/?name=R-T-I&background=ed4626&color=ffffff&rounded=true&length=3'}}">
    <link rel="stylesheet" href="{{ asset('assets/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/croppie.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/sweetalert2.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/datatables.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/summernote.css')}}">

    <script src="{{ asset('assets/js/material-ripple.js')}}"></script>
    <script src="{{ asset('assets/js/layout-helpers.js')}}"></script>
    <script src="{{ asset('assets/js/theme.js')}}"></script>
    <script src="{{ asset('assets/js/pace.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.min.js')}}"></script>

    @stack('stylesheets')
  </head>
  <body>
    <div class="page-loader">
      <div class="bg-background"></div>
    </div>
    <div class="layout-wrapper layout-2">
      <div class="layout-inner">
        <div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-menu">
          @include('includes/header')
          @include('includes/menu')
        </div>
        <div class="layout-container">
          <nav class="layout-navbar navbar navbar-expand-lg align-items-lg-center bg-white container-p-x" id="layout-navbar">
            <div class="layout-sidenav-toggle navbar-nav d-lg-none align-items-lg-center">
              <a class="nav-item nav-link px-0 mr-lg-4" href="javascript:void(0)">
                <i class="ion ion-md-menu text-large align-middle"></i>
              </a>
            </div>
            <a href="{{route('manage.beranda')}}" class="navbar-brand app-brand logo d-lg-none py-0 mr-4">
              <span class="app-brand-text logo font-weight-normal ml-2" >Rapier CMS</span>
            </a>
            <div class="navbar-nav align-items-lg-center ml-auto bless">
              <div class="demo-navbar-notifications nav-item dropdown">
                <a class="nav-link dropdown-toggle hide-arrow" href="#" data-toggle="dropdown" aria-expanded="false">
                  <i class="ion ion-md-notifications-outline navbar-icon align-middle"></i>
                  <span class="badge badge-primary badge-dot indicator"></span>
                  <span class="d-lg-none align-middle">&nbsp;</span>
                </a>
                   <div class="dropdown-menu dropdown-menu-right">
                  <div class="bg-background text-center text-white font-weight-bold p-3">
                    4 Pesan Baru
                  </div>
                  <div class="list-group list-group-flush">
                    <a href="javascript:void(0)" class="list-group-item list-group-item-action media d-flex align-items-center">
                    
                      <div class="media-body line-height-condenced ml-3">
                        <div class="text-body">Login from 192.168.1.1</div>
                        <div class="text-light small mt-1">
                          Aliquam ex eros, imperdiet vulputate hendrerit et.
                        </div>
                        <div class="text-light small mt-1">12h ago</div>
                      </div>
                    </a>

                    <a href="javascript:void(0)" class="list-group-item list-group-item-action media d-flex align-items-center">
                     
                      <div class="media-body line-height-condenced ml-3">
                        <div class="text-body">You have <strong>4</strong> new followers</div>
                        <div class="text-light small mt-1">
                          Phasellus nunc nisl, posuere cursus pretium nec, dictum vehicula tellus.
                        </div>
                      </div>
                    </a>

                    <a href="javascript:void(0)" class="list-group-item list-group-item-action media d-flex align-items-center">
                    
                      <div class="media-body line-height-condenced ml-3">
                        <div class="text-body">Server restarted</div>
                        <div class="text-light small mt-1">
                          19h ago
                        </div>
                      </div>
                    </a>

                    <a href="javascript:void(0)" class="list-group-item list-group-item-action media d-flex align-items-center">
                    
                      <div class="media-body line-height-condenced ml-3">
                        <div class="text-body">99% server load</div>
                        <div class="text-light small mt-1">
                          Etiam nec fringilla magna. Donec mi metus.
                        </div>
                        <div class="text-light small mt-1">
                          20h ago
                        </div>
                      </div>
                    </a>
                  </div>

                  <a href="javascript:void(0)" class="d-block text-center text-light small p-2 my-1">Lihat Pesan Lainnya</a>
                  </div>
              </div>
              <div class="demo-navbar-user nav-item dropdown">

                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                  <span class="d-inline-flex flex-lg-row-reverse align-items-center align-middle">
                    <img src="{{session('profile')}}" alt class="d-block ui-w-30 rounded-circle">
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="{{route('profil.index')}}" class="dropdown-item"><i class="ion ion-ios-person text-lightest"></i> &nbsp; Profil</a>
                  <a href="{{route('profil.password')}}" class="dropdown-item"><i class="ion ion-ios-mail text-lightest"></i> &nbsp; Ganti Password</a>
                  <div class="dropdown-divider"></div>
                  <a href="{{route('manage.logout')}}" class="dropdown-item"><i class="ion ion-ios-log-out text-danger"></i> &nbsp; Log Out</a>
                </div>
              </div>
            </div>
          </nav>          
          <div class="layout-content">
          <div class="container-fluid flex-grow-1 container-p-y">
                @yield('main_container')
          </div>
          <nav class="layout-footer footer bg-footer-theme">
            <div class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
              <div class="pt-3">
                <span class="footer-text font-weight-bolder">2020</span> © {{$perusahaan->nama}} Support By <a href="https://rapiertechnology.co.id/" target="_blank">PT Rapier Technology International</a>
              </div>
                
            </div>
          </nav>
        </div>
      </div>
    </div>
    <div class="layout-overlay layout-sidenav-toggle"></div>
  </div>
    <script src="{{ asset('assets/js/popper.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.js')}}"></script>
    <script src="{{ asset('assets/js/sidenav.js')}}"></script>
    <script src="{{ asset('assets/js/perfect-scrollbar.js')}}"></script>
    <script src="{{ asset('assets/js/custom.js')}}"></script>
    <script src="{{ asset('assets/js/menu_active.js')}}"></script>
    <script src="{{ asset('assets/js/sweetalert.min.js')}}"></script>
    <script src="{{ asset('assets/js/croppie.js')}}"></script>
    <script src="{{ asset('assets/js/datatables.js')}}"></script>
  
    <script type="text/javascript" src="{{ asset('assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/moment.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.validate.js')}}"></script>
    <script src="{{ asset('assets/js/summernote.js')}}"></script>
    <script src="{{ asset('assets/js/additional-methods.js')}}"></script>
    @stack('scripts')
  </body>
</html>