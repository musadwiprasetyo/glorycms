@extends('layouts.backend')
@section('pageTitle', "Detail Blog | $perusahaan->nama")
@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/users.css')}}">
@endpush
@section('main_container')
<div class="layout-content">
  <div class="container-fluid flex-grow-1 container-p-y">

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('manage.beranda')}}">Beranda</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('blog.index')}}">Blog</a>
      </li>
      <li class="breadcrumb-item active">Detail </li>
    </ol>

    <h4 class="font-weight-bold py-3 mb-4">
     <span class="text-muted">Detail Blog</span>
    </h4>

    <div class="nav-tabs-top">
     
      <div class="card">
        <div class="card-body pb-2">
          <form class="form-horizontal" id="submitData">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="media align-items-center">
            <img src="{{isset($blog)? $gambar : url('media/blog/no_img.png')}}" alt="" id="ShowAvatar" class="d-block ui-w-400">
            </div>

            <br/>
            <hr/>
            <br>

            <table class="table user-view-table m-0">  
              <tbody>
                <tr>
                  <td>Judul</td>
                  <td>: {{$blog->title}}</td>
                </tr>
                <tr>
                  <td>Deskripsi</td>
                  <td>: {!!$blog->description!!}</td>
                </tr>
                <tr>
                  <td>Meta Title</td>
                  <td>: {{$blog->meta_title}}</td>
                </tr>
                <tr>
                  <td>Meta Title</td>
                  <td>: {{$blog->meta_description}}</td>
                </tr>
                <tr>
                  <td>Keyword</td>
                  <td>: {{$blog->keyword}}</td>
                </tr>
                <tr>
                  <td>Jumlah Pembaca</td>
                  <td>: {{$blog->read}}</td>
                </tr>
                <tr>
                  <td>Status</td>
                  <td>: {!!$status!!}</td>
                </tr>
              </tbody>
            </table>   

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')

@endpush
   