@extends('layouts.backend')
@section('pageTitle', "Manajemen Blog | $perusahaan->nama")
@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/users.css')}}">
@endpush
@section('main_container')
<div class="layout-content">
  <div class="container-fluid flex-grow-1 container-p-y">

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('manage.beranda')}}">Beranda</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('blog.index')}}">Blog</a>
      </li>
      <li class="breadcrumb-item active">{{isset($blog) ? 'Edit' : 'Tambah'}} </li>
    </ol>

    <h4 class="font-weight-bold py-3 mb-4">
    {{isset($blog) ? 'Edit' : 'Tambah'}}  <span class="text-muted">Blog</span>
    </h4>

    @if(session('message'))
    <div class="alert alert-{{session('message')['status']}}">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message')['desc'] }}
    </div>
    @endif

    <div class="nav-tabs-top">
     
      <div class="card">
        <div class="card-body pb-2">
          <form class="form-horizontal" id="submitData">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="enc_id" id="enc_id" value="{{isset($blog)? $enc_id : ''}}">
            
            <div class="media align-items-center">
            <img src="{{isset($blog)? $gambar : url('media/blog/no_img.png')}}" alt="" id="showgambar" class="d-block ui-w-400">
              <div class="media-body ml-3">
                <label class="form-label d-block mb-2">Gambar Blog</label>
                <label class="btn btn-tambah btn-sm">
                  Ubah
                  <input type="file" class="user-edit-fileinput" id="gambar" name="gambar">
                </label>
                  <input type="hidden" name="image" value="" id="image">
              </div>
            </div>

            <br/>
            <hr class="border-light m-0">
            <br/>

            <div class="form-row">
              <div class="form-group col-md-12">
                <label class="form-label">Judul <span>*</span></label>
                <input type="text" class="form-control mb-1" name="title" id="title" value="{{isset($blog)? $blog->title : ''}}">
              </div>
            </div>
            <div class="form-group">
              <label class="form-label">Deskripsi <span>*</span></label>
              <textarea class="form-control textarea editor" rows="5" name="description" id="description" required="">{{isset($blog)? ($blog->description==null?'Ini Deskripsi Blog':$blog->description) : 'Ini Deskripsi Blog'}}</textarea>
            </div>
            <br/>
            <hr class="border-light m-0">
            <br/>
            <h6 class="mb-4">Meta</h6>
            <div class="form-group">
              <label class="form-label">Meta Title </label>
              <input type="text" class="form-control" id="meta_title" name="meta_title" value="{{isset($blog)? $blog->meta_title : ''}}">
            </div>

            <div class="form-group">
              <label class="form-label">Meta Dekripsi</label>
             <textarea class="form-control" rows="3" name="meta_description" id="meta_description">{{isset($blog)? $blog->meta_description : ''}}</textarea>
            </div>

            <div class="form-group">
              <label class="form-label">Keyword</label>
              <input type="text" class="form-control" id="keywords" name="keywords" value="{{isset($blog)? $blog->keyword : ''}}" placeholder="pisahkan dengan koma(,)">
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <label class="form-label">Status </label>
                <select name="status" class="custom-select" id="status">
                  @foreach($status as $key => $row)
                  <option value="{{$key}}"{{ $selectedstatus == $key ? 'selected=""' : '' }}>{{ucfirst($row)}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <div class="text-right mt-3">
                  <button type="submit" class="btn btn-simpan" id="simpan">Simpan</button>&nbsp;
                  <a href="{{route('blog.index')}}"  class="btn btn-default">Kembali</a>
                </div>
              </div>
            </div>

          </form>

          <div class="modal modal-fill-in" id="Loading">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                  <div class="card-body text-center" id="loadingbro">
                    <div class="demo-inline-spacing">
                      <div class="spinner-grow" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

           <div id="modalUpload" class="modal" role="dialog">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                <div class="modal-header">
                  <h6 class="modal-title">Upload Gambar</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <div id="image_demo" style="width:250px; margin-top:30px"></div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-simpan crop_image">Proses</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  @endsection
@push('scripts')
<script type="text/javascript">
 
  $('#submitData').validate({
    ignore: ":hidden:not(.editor)",
    rules: {
      title:{
        required: true
      },
      description:{
        required: true
      }
    },
    messages: {
      title: {
        required: "Judul Blog tidak boleh kosong"
      },
      description: {
        required: "Deskripsi blog tidak boleh kosong",
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
      console.log(element.closest('.form-group').append(error));
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: function(form) {
      SimpanData();
    }
  });
   function SimpanData(){    
        $('#simpan').addClass("disabled");
         var enc_id         =$('#enc_id').val();
         var title          =$('#title').val();
         var description    =$('#description').val();
         var image           =$('#image').val();
         var meta_title      =$('#meta_title').val();
         var meta_description=$('#meta_description').val();
         var keywords        =$('#keywords').val();
         var status          =$('#status').val();
      
         var dataFile = new FormData()
       
         dataFile.append('image', image);
         dataFile.append('enc_id', enc_id);
         dataFile.append('title', title);
      
         dataFile.append('description', description);
         dataFile.append('meta_title', meta_title);
         dataFile.append('meta_description', meta_description);
         dataFile.append('keywords', keywords);
         dataFile.append('status', status);
        
        $.ajax({
          type: 'POST',
          url : "{{route('blog.simpan')}}",
          headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
          data:dataFile,
          processData: false,
          contentType: false,
          dataType: "json",
          beforeSend: function () {
              $('#Loading').modal('show');
          },
          success: function(data){
            if (data.success) {
               Swal.fire('Yes',data.message,'info');
               window.location.reload();
            } else {
               Swal.fire('Ups',data.message,'info'); 
            }
            
          },
          complete: function () { 
             $('#simpan').removeClass("disabled");
             $('#Loading').modal('hide');
          },
          error: function(data){
               $('#simpan').removeClass("disabled");
               $('#Loading').modal('hide');
              console.log(data);
          }
        });
    }
  
   $(document).ready(function(){
    $('.textarea').summernote({
            height: 200,
            disableDragAndDrop: true,
            defaultFontName: 'Nunito',
            fontNamesIgnoreCheck: ["Nunito"],
            fontNames: ["Nunito"],   
            fontSizeUnits: ['px'],
            fontSizes: ['8', '9', '10', '11', '12', '13','14','15','18', '24', '36', '48' , '64', '82', '150'],
            toolbar: [
                        ['style', ['style']],
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ["fontname", ["fontname"]],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ["para", ["ul", "ol", "paragraph"]],
                        ["table", ["table"]],
                        ['insert', ['link', 'picture','video','hr']],
                        ['height', ['height']],
                        ['view', ['fullscreen', 'codeview', 'help']],
                      ]       
        });
       $('.textarea').summernote('fontSize', 15);
       $('.textarea').summernote('fontName', 'Nunito');

   $('#tgl_lahir').bootstrapMaterialDatePicker({
    weekStart: 0,
    time: false,
    format : 'DD-MM-YYYY',
    clearButton: true
  });
   $image_crop = $('#image_demo').croppie({
      enableExif: true,
      mouseWheelZoom: true,
      viewport: {
        width:800,
        height:400,
        type:'square'
      },
      boundary:{
        width:900,
        height:500
      }
    });
    $('.crop_image').click(function(event){
      $image_crop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function(response){
         $('#modalUpload').modal('hide');
          $('#showgambar').attr('src', response);
          document.getElementById("image").value = response;
      })
    });

   $('#gambar').on('change', function(){
      var reader = new FileReader();
      reader.onload = function (event) {
        $image_crop.croppie('bind', {
          url: event.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });
      }
      reader.readAsDataURL(this.files[0]);
      console.log(this.files[0]);
      $('#modalUpload').modal('show');
    });

 });
</script>
@endpush
   