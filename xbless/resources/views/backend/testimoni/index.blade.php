@extends('layouts.backend')
@section('pageTitle', "Data Testimoni | $perusahaan->nama")
@push('stylesheets')
@endpush
@section('main_container')
<div class="container-fluid flex-grow-1 container-p-y">

  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{{route('manage.beranda')}}">Beranda</a>
    </li>
    <li class="breadcrumb-item active">
      <a href="javascript:void(0)">Testimoni</a>
    </li>
  </ol>

  <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
  <div>Testimoni</div>
  @can('testimoni.tambah')
  <a href="{{ route('testimoni.tambah')}}" class="btn btn-tambah d-block"><span class="ion ion-md-add"></span>&nbsp; Tambah Testimoni</a>
  @endcan
  </h4>

  @if(session('message'))
  <div class="alert alert-{{session('message')['status']}}">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {{ session('message')['desc'] }}
  </div>
  @endif
    

  <div class="card">
    <div class="card-datatable table-responsive">
      <table id="table1" class="table display table-striped table-bordered">
        <thead>
          <tr>
            <th width='1%'>No</th>
            <th width="25%">Nama</th>
            <th width="30%">Job</th>
            <th width="10%">Urutan</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
      </table>
    </div>
  </div>

  <div class="modal fade" id="modalData">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="card-body table-responsive p-0">
              <table id="orderData" class="table  table-hover" width="100%">
                <thead>
                  <tr>
                    <th class="text-center no-sort" width="1px">No</th>
                    <th class="text-left no-sort" width="100px;">Nama Kolom</th>
                    <th class="text-left no-sort">Nilai</th>
                  </tr>
                </thead>
                <tbody id="tbodyOrderDetailData">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

</div>
@endsection
@push('scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.7/css/rowReorder.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript">
   var table,tabledata,table_index;
      $(document).ready(function(){
          $.ajaxSetup({
              headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
          });
          table= $('#table1').DataTable({
          "processing": true,
          "serverSide": true,
          "stateSave"  : true,
          "deferRender": true,
          "pageLength": 25,
          "select" : true,
          "rowReorder": true,
          "ajax":{
                   "url": "{{ route("testimoni.getdata") }}",
                   "dataType": "json",
                   "type": "POST",
                   data: function ( d ) {
                     d._token= "{{csrf_token()}}";
                   }
                 },
          "columns": [
              { 
                "data": "no",
                "orderable" : false,
              },
              { "data": "name" },
              { "data": "job",},
              { "data": "sort" },
              { "data": "status" },
              { "data" : "action",
                "orderable" : false,
                "className" : "text-center",
              },
          ],
          responsive: true,
          language: {
              search: "_INPUT_",
              searchPlaceholder: "Cari data",
              emptyTable: "Belum ada data",
              info: "Menampilkan data _START_ sampai _END_ dari _MAX_ data.",
              infoEmpty: "Menampilkan 0 sampai 0 dari 0 data.",
              lengthMenu: "Tampilkan _MENU_ data per halaman",
              loadingRecords: "Loading...",
              processing: "Mencari...",
              paginate: {
                "first": "Pertama",
                "last": "Terakhir",
                "next": "Sesudah",
                "previous": "Sebelum"
              },
          }
        });
        table.on( 'row-reorder', function ( e, diff, edit ) {
             console.log(diff[0].newPosition+1);
             console.log(edit.triggerRow.data()['id']);
             var id = edit.triggerRow.data()['id'];
           
             var urutan = [];
              for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                urutan[i] = {
                 id:  diff[i].node.id,
                 sort: diff[i].newPosition+1,
                };
              }
              console.log(urutan);
              $.ajax({
                      url: "{{route("testimoni.sorting",[null])}}/" + id,
                      method: "GET",
                      data: { "sorting" : urutan },
                      dataType: "json",
                      success: function(data) {
                         table.ajax.reload(null, true);
                      },
                      error: function(data) {}
                 });
        });

        tabledata = $('#orderData').DataTable({
          dom     : 'lrtp',
          paging    : false,
          columnDefs: [ {
                "targets": 'no-sort',
                "orderable": false,
          } ]
        });
        
        $('#table1 tbody').on('dblclick', 'tr', function(){
        var data = table.row(this).data();
        @cannot('testimoni.detail')
          swal('Ups!', "Anda tidak memiliki HAK AKSES! Hubungi ADMIN Anda.",'error'); return false;
        @else
        console.log(data);
        tabledata.clear();
        tabledata.rows.add([
        {'0':'<div class="text-right">1.</div>', '1':'Nama', '2':data.name},
        {'0':'<div class="text-right">2.</div>', '1':'Job', '2':data.job},
        {'0':'<div class="text-right">3.</div>', '1':'Isi', '2':data.content},
        {'0':'<div class="text-right">4.</div>', '1':'Urutan', '2':data.sort},
        {'0':'<div class="text-right">5.</div>', '1':'Status', '2':data.status},
        {'0':'<div class="text-right">6.</div>', '1':'Tgl. Buat', '2':data.tgl}
      ]);
      tabledata.draw();
      $('#modalData').modal('show');
      @endcannot
        });
        table.on('select', function ( e, dt, type, indexes ){
          table_index = indexes;
          var rowData = table.rows( indexes ).data().toArray();  
        });
      });
        function detailData(e,key){
          var data = table.row(key).data();
           @cannot('testimoni.detail')
            swal('Ups!', "Anda tidak memiliki HAK AKSES! Hubungi ADMIN Anda.",'error'); return false;
           @else
              console.log(data);
              tabledata.clear();
              tabledata.rows.add([
              {'0':'<div class="text-right">1.</div>', '1':'Nama', '2':data.name},
              {'0':'<div class="text-right">2.</div>', '1':'Job', '2':data.job},
              {'0':'<div class="text-right">3.</div>', '1':'Isi', '2':data.content},
              {'0':'<div class="text-right">4.</div>', '1':'Urutan', '2':data.sort},
              {'0':'<div class="text-right">5.</div>', '1':'Status', '2':data.status},
              {'0':'<div class="text-right">6.</div>', '1':'Tgl. Buat', '2':data.tgl}
            ]);
            tabledata.draw();
            $('#modalData').modal('show');
            @endcannot
          
        }
        function deleteData(e,enc_id){
          @cannot('testimoni.hapus')
              Swal.fire('Ups!', "Anda tidak memiliki HAK AKSES! Hubungi ADMIN Anda.",'error'); return false;
          @else
          var token = '{{ csrf_token() }}';
          Swal.fire({
            title: "Apakah Anda yakin?",
            text: "Data akan terhapus!",
          
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ya",
            cancelButtonText:"Batal",
            confirmButtonColor: "#ec6c62",
            closeOnConfirm: false
          }).then(function(result) {
          if (result.value) {
            $.ajaxSetup({
              headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
            });
             $.ajax({
              type: 'DELETE',
              url: '{{route("testimoni.hapus",[null])}}/' + enc_id,
              headers: {'X-CSRF-TOKEN': token},
              success: function(data){
                if (data.status=='success') {
                    Swal.fire('Yes',data.message,'success');
                    table.ajax.reload(null, true);
                 }else{
                   Swal.fire('Ups',data.message,'info');
                 }
              },
              error: function(data){
                console.log(data);
                Swal.fire("Ups!", "Terjadi kesalahan pada sistem.", "error");
              }
            });

           
          } else {
           
          }
         });
          @endcannot
      }
      $(document.body).on("keydown", function(e){
        ele = document.activeElement;
          if(e.keyCode==38){
            table.row(table_index).deselect();
            table.row(table_index-1).select();
          }
          else if(e.keyCode==40){
              
            table.row(table_index).deselect();
            table.rows(parseInt(table_index)+1).select();
            console.log(parseInt(table_index)+1);
              
          }
          else if(e.keyCode==13){
            var data = table.row(table_index).data();
            @cannot('testimoni.detail')
              swal('Ups!', "Anda tidak memiliki HAK AKSES! Hubungi ADMIN Anda.",'error'); return false;
            @else
              console.log(data);
              tabledata.clear();
              tabledata.rows.add([
              {'0':'<div class="text-right">1.</div>', '1':'Nama', '2':data.name},
              {'0':'<div class="text-right">2.</div>', '1':'Job', '2':data.job},
              {'0':'<div class="text-right">3.</div>', '1':'Isi', '2':data.content},
              {'0':'<div class="text-right">4.</div>', '1':'Urutan', '2':data.sort},
              {'0':'<div class="text-right">5.</div>', '1':'Status', '2':data.status},
              {'0':'<div class="text-right">6.</div>', '1':'Tgl. Buat', '2':data.tgl}
            ]);
            tabledata.draw();
            $('#modalData').modal('show');
          @endcannot
          }
      });
</script>
@endpush
   