@extends('layouts.backend')
@section('pageTitle', "Manajemen FAQ | $perusahaan->nama")
@push('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/users.css')}}">
@endpush
@section('main_container')
<div class="layout-content">
  <div class="container-fluid flex-grow-1 container-p-y">

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('manage.beranda')}}">Beranda</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('faq.index')}}">FAQ</a>
      </li>
      <li class="breadcrumb-item active">{{isset($faq) ? 'Edit' : 'Tambah'}} </li>
    </ol>

    <h4 class="font-weight-bold py-3 mb-4">
    {{isset($faq) ? 'Edit' : 'Tambah'}}  <span class="text-muted">FAQ</span>
    </h4>

    @if(session('message'))
    <div class="alert alert-{{session('message')['status']}}">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message')['desc'] }}
    </div>
    @endif

    <div class="nav-tabs-top">
     
      <div class="card">
        <div class="card-body pb-2">
          <form class="form-horizontal" id="submitData">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="enc_id" id="enc_id" value="{{isset($faq)? $enc_id : ''}}">
            <div class="form-row">
              <div class="form-group col-md-12">
                <label class="form-label">Pertanyaan <span>*</span></label>
                <input type="text" class="form-control mb-1" name="question" id="question" value="{{isset($faq)? $faq->question : ''}}">
              </div>
            </div>

            <div class="form-group">
              <label class="form-label">Jawaban <span>*</span></label>
              <textarea class="form-control textarea editor" rows="3" name="answer" id="answer">{{isset($faq)? ($faq->answer==null?'Isi dengan jawaban pertanyaan':$faq->answer) : 'Isi dengan jawaban pertanyaan'}}</textarea>
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <label class="form-label">Status </label>
                <select name="status" class="custom-select" id="status">
                  @foreach($status as $key => $row)
                  <option value="{{$key}}"{{ $selectedstatus == $key ? 'selected=""' : '' }}>{{ucfirst($row)}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <div class="text-right mt-3">
                  <button type="submit" class="btn btn-simpan" id="simpan">Simpan</button>&nbsp;
                  <a href="{{route('faq.index')}}"  class="btn btn-default">Kembali</a>
                </div>
              </div>
            </div>

          </form>

          <div class="modal modal-fill-in" id="Loading">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                  <div class="card-body text-center" id="loadingbro">
                    <div class="demo-inline-spacing">
                      <div class="spinner-grow" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                      <div class="spinner-grow text-danger" role="status">
                        <span class="sr-only">Loading...</span>
                      </div>
                    </div>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

           
          
        </div>
      </div>
    </div>
  </div>
  @endsection
@push('scripts')
<script type="text/javascript">
 
  $('#submitData').validate({
    ignore: ":hidden:not(.editor)",
    rules: {
      question:{
        required: true
      },
      answer:{
        required: true
      }
    },
    messages: {
      question: {
        required: "Pertanyaan tidak boleh kosong"
      },
       answer: {
        required: "Jawaban tidak boleh kosong"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
      console.log(element.closest('.form-group').append(error));
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: function(form) {
      SimpanData();
    }
  });
   function SimpanData(){    
        $('#simpan').addClass("disabled");
         var enc_id          =$('#enc_id').val();
        
         var question        =$('#question').val();
         var answer          =$('#answer').val();
         var status          =$('#status').val();
      
         var dataFile = new FormData()
       
         dataFile.append('enc_id', enc_id);
         dataFile.append('question', question);
         dataFile.append('answer', answer);
         dataFile.append('status', status);
        
        $.ajax({
          type: 'POST',
          url : "{{route('faq.simpan')}}",
          headers: {'X-CSRF-TOKEN': $('[name="_token"]').val()},
          data:dataFile,
          processData: false,
          contentType: false,
          dataType: "json",
          beforeSend: function () {
              $('#Loading').modal('show');
          },
          success: function(data){
            if (data.success) {
                Swal.fire('Yes',data.message,'info');
                window.location.href="{{ route('faq.index') }}";
            } else {
               Swal.fire('Ups',data.message,'info'); 
            }
            
          },
          complete: function () { 
             $('#simpan').removeClass("disabled");
             $('#Loading').modal('hide');
          },
          error: function(data){
               $('#simpan').removeClass("disabled");
               $('#Loading').modal('hide');
              console.log(data);
          }
        });
    }
  
   $(document).ready(function(){
    
      $('.textarea').summernote({
          height: 200,
          disableDragAndDrop: true,
          defaultFontName: 'Nunito',
          fontNamesIgnoreCheck: ["Nunito"],
          fontNames: ["Nunito"],   
          fontSizeUnits: ['px'],
          fontSizes: ['8', '9', '10', '11', '12', '13','14','15','18', '24', '36', '48' , '64', '82', '150'],
          toolbar: [
                      ['style', ['style']],
                      ['style', ['bold', 'italic', 'underline', 'clear']],
                      ['font', ['strikethrough', 'superscript', 'subscript']],
                      ["fontname", ["fontname"]],
                      ['fontsize', ['fontsize']],
                      ['color', ['color']],
                      ["para", ["ul", "ol", "paragraph"]],
                      ["table", ["table"]],
                      ['insert', ['link', 'picture','video','hr']],
                      ['height', ['height']],
                      ['view', ['fullscreen', 'codeview', 'help']],
                    ]       
      });

      $('.textarea').summernote('fontSize', 15);
      $('.textarea').summernote('fontName', 'Nunito');

 });
</script>
@endpush
   